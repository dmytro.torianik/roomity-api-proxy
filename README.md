# Roomity API Proxy

NGINX proxy app for Roomity API

## Usage

### Environment Variables

-   `LISTEN_PORT` - port to list on (default: `8000`)
-   `APP_HOST` - hostname of the app to forward requests to (default: `app`)
-   `APP_PORT` - port of the app to forward requests to (default: `9000`)
